FROM node:11 as builder

#build content
RUN mkdir /src
WORKDIR /src
COPY . /src
RUN npm i --silent
RUN npm run build

#production image
FROM nginx:alpine
RUN rm -rf /etc/nginx/conf.d
COPY deploy/conf /etc/nginx

COPY --from=builder /src/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
