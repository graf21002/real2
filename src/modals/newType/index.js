import React, { useState } from 'react'
import { Modal } from 'antd'
import { connect } from 'react-redux'
import { deactivate } from '../../action/modal'
import { addNewType } from '../../action'
import { Input, RadioInputs } from '../../components'

let NAME = 'newType'

const mapStateToProps = (state) => {
  let { modal } = state
  return { active: modal[NAME] }
}

const mapDispatchToProps = dispatch => {
  return {
    onCancel: () => {
      dispatch(deactivate(NAME))
    },
    onOk: ({ name, type }) => {
      dispatch(addNewType(name, type))
      dispatch(deactivate(NAME))
    }
  }
}

const NewType = ({ active, onCancel, onOk }) => {
  let [name, setName] = useState('')
  let [type, setType] = useState('objectType')
  let [error, setError] = useState(false)
  let submit = () => {
    if (name) {
      onOk({ name, type })
      setName('')
      setType('objectType')
      setError(false)
    } else {
      setError(true)
    }
  }
  return (
    <Modal
      title="New Type"
      visible={active}
      onCancel={onCancel}
      closable={false}
      footer={[
        <button key="submit" className={'btn btn-secondary dart-btn'} onClick={submit}>
          Ok
        </button>,
        <button key="back" className={'btn btn-outline-secondary dart-btn-outline'} onClick={onCancel}>Cancel</button>
      ]}
    >
      <form onSubmit={e => {
        e.preventDefault()
        submit()
      }}>
        <div className={'row'}>
          <div className={'col-9'}>
            <Input type={'text'} value={name} onChange={setName} label={'Name'}
                   id={'modalNewTypeName'}/>
            {error && <p className={'text-danger'}>
              Please provide a name.
            </p>}
          </div>
          <div className={'col-3 pt-2'}><RadioInputs values={['objectType', 'table']} active={type}
                                                     handelChange={setType}/></div>
        </div>
      </form>
    </Modal>
  )
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(NewType)
