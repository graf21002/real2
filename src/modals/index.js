import React, { Fragment } from 'react'
import NewType from './newType'
import NewProperty from './newProperty'

const Modals = () => {
  return (
    <Fragment>
      <NewType/>
      <NewProperty/>
    </Fragment>
  )
}

export default Modals