import React, { useState } from 'react'
import { Modal } from 'antd'
import { connect } from 'react-redux'
import { deactivate } from '../../action/modal'
import { addPropOnType } from '../../action'
import { Input, RadioInputs, Select } from '../../components'

let NAME = 'newProperty'

const mapStateToProps = (state) => {
  let { modal } = state
  console.log(modal.other, modal)
  return { active: modal[NAME], index: modal.other }
}

const mapDispatchToProps = dispatch => {
  return {
    onCancel: () => {
      dispatch(deactivate(NAME))
    },
    onOk: ({ name, type, select, index }) => {
      dispatch(addPropOnType(name, type, select, index))
      dispatch(deactivate(NAME))
    }
  }
}

const NewProperty = ({ active, index, onCancel, onOk }) => {
  let [name, setName] = useState('')
  let [type, setType] = useState('single')
  let [option, setOption] = useState('editable')
  let [error, setError] = useState(false)
  let [select, setSelect] = useState('text')
  let submit = () => {
    if (name) {
      onOk({ name, type, select, index })
      setName('')
      setType('single')
      setOption('editable')
      setSelect('text')
      setError(false)
    } else {
      setError(true)
    }
  }
  return (
    <Modal
      title="New Property"
      visible={active}
      onCancel={onCancel}
      closable={false}
      width={410}
      footer={[
        <button key="submit" className={'btn btn-secondary dart-btn'} style={{ width: 125 }} onClick={submit}>
          Ok
        </button>,
        <button key="back" className={'btn btn-outline-secondary dart-btn-outline'} style={{ width: 125 }}
                onClick={onCancel}>Cancel</button>
      ]}
    >
      <form onSubmit={e => {
        e.preventDefault()
        submit()
      }}>
        <div className={'row'}>
          <div className={'col-3'}><RadioInputs label={'TYPE'} values={['single', 'listOf']} active={type}
                                                handelChange={setType} name={'typeNewProps'}/></div>
          <div className={'col-3 mt-2'}><RadioInputs values={['editable', 'computed']} active={option}
                                                     handelChange={setOption} name={'optionNewProps'}/></div>
          <div className={'col-6'}><Select values={['text', 'template', 'selection']} value={select}
                                           onChange={setSelect}/>
          </div>
          <div className={'col-12'}>
            <Input type={'text'} value={name} onChange={setName} label={'Name'}
                   id={'modalNewTypeName'}/>
            {error && <p className={'text-danger'}>
              Please provide a name.
            </p>}
          </div>
        </div>
      </form>
    </Modal>
  )
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(NewProperty)
