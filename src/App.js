import React, { Component, } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import store from './createStore'
import routes from './routes'
import { Container } from './components'
import Modals from './modals'

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <Router>
          <Container>
            <Switch>
              {routes.map(route => <Route key={'route' + route.path} exact path={route.path}
                                          component={route.component}/>)}
            </Switch>
          </Container>

        </Router>
        <Modals/>
      </Provider>
    )
  }
}

export default App
