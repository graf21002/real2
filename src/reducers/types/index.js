import { CHANGE_TYPE_NAME, ADD_NEW_TYPE, ADD_PROP_ON_TYPE, CHANGE_PROP_ON_TYPE } from '../../action'

function types (state = [{ name: 'test', props: [] }], action) {
  switch (action.type) {
    case ADD_NEW_TYPE: {
      return [...state, {
        name: action.name,
        props: []
      }]
    }
    case ADD_PROP_ON_TYPE: {
      if (state[action.index].props) {
        state[action.index].props = [...state[action.index].props, {
          name: action.name,
          type: action.select,
          label: ''
        }]
      } else {
        state[action.index].props = [{
          name: action.name,
          type: action.select,
          label: ''
        }]
      }
      return [...state]
    }
    case CHANGE_TYPE_NAME: {
      state[action.index][action.key] = action.val
      return [...state]
    }
    case CHANGE_PROP_ON_TYPE: {
      state[action.index].props[action.prop][action.key] = action.val
      return [...state]
    }
    default: {
      return state
    }
  }
}

export default types