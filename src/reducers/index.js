import { combineReducers } from 'redux'
import types from './types'
import modal from './modal'

const rootReducer = combineReducers({
  types,
  modal
})

export default rootReducer