export const ADD_NEW_TYPE = 'ADD_NEW_TYPE'
export const CHANGE_TYPE_NAME = 'CHANGE_TYPE_NAME'
export const ADD_PROP_ON_TYPE = 'ADD_PROP_ON_TYPE'
export const CHANGE_PROP_ON_TYPE = 'CHANGE_PROP_ON_TYPE'

export const addNewType = (name, option) => ({ type: ADD_NEW_TYPE, name, option })
export const addPropOnType = (name, typeProps, select, index) => ({
  type: ADD_PROP_ON_TYPE,
  name,
  typeProps,
  select,
  index
})
export const changeTypeName = (val, key, index) => ({ type: CHANGE_TYPE_NAME, val, index, key })
export const changePropOnType = (index, prop, key, val) => ({ type: CHANGE_PROP_ON_TYPE, prop, key, val, index })


