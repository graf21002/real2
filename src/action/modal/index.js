export const ACTIVE_MODAL = 'ACTIVE_MODAL'
export const DEACTIVATE_MODAL = 'DEACTIVATE_MODAL'

export const active = (key, other) => ({ type: ACTIVE_MODAL, key, other })
export const deactivate = key => ({ type: DEACTIVATE_MODAL, key })