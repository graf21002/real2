import {
  ADD_NEW_TYPE,
  ADD_PROP_ON_TYPE,
  CHANGE_PROP_ON_TYPE,
  CHANGE_TYPE_NAME,
  changeTypeName,
  changePropOnType,
  addNewType,
  addPropOnType,
} from './type'

export {
  ADD_NEW_TYPE,
  ADD_PROP_ON_TYPE,
  CHANGE_PROP_ON_TYPE,
  CHANGE_TYPE_NAME,
  changeTypeName,
  changePropOnType,
  addNewType,
  addPropOnType
}