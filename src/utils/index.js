export const InferLabelFromName = name => !name ? name :
  name[0] + name.slice(1)
    .replace(/([a-z\u0430-\u045F])([A-Z\u0401-\u042F]|[0-9])/g, '$1 $2')
    .replace(/([A-Z\u0401-\u042F])([a-z\u0430-\u045F])/g, ' $1$2')
    .replace(/ +/g, ' ')
    .toLowerCase()
