import React from 'react'
import PropTypes from 'prop-types'

import { Input, Select } from '../../../components'
import { Text, TEXT } from './text'
import { SELECTION, Selection } from './selection'
import { InferLabelFromName } from '../../../utils'

Property.propTypes = {
  prop: PropTypes.object,
  changeProp: PropTypes.func,
}

function Property ({ prop: { name, label, type, relevance, help, template, pattern,question, lines, suggestions, prefix, textMayContainLineBreaks, allowFormatting }, changeProp }) {
  switch (type) {
    case TEXT: {
      return (
        <Text prop={{
          name,
          label,
          type,
          relevance,
          help,
          pattern,
          lines,
          suggestions,
          textMayContainLineBreaks,
          allowFormatting, prefix
        }} changeProp={changeProp}/>)
    }
    case SELECTION: {
      return (
        <Selection prop={{
          name,
          label,
          type,
          relevance,
          help,
          pattern,
          question,
          suggestions,
          textMayContainLineBreaks,
          allowFormatting, prefix
        }} changeProp={changeProp}/>)
    }
    case 'template': {
      return (<div className={'w-75'}>
        <div className={'row'}>
          <div className={'col-6'}>
            <Input label={'PROPERTY NAME'} value={name} onChange={val => changeProp('name', val)} id={'propertyName'}
                   type={'text'} spellcheck={false}/>
          </div>
          <div className={'col-6'}>
            <Select label={'DATA TYPE'} value={type} id={'dataType'} onChange={val => changeProp('type', val)}
                    values={['text', 'template','selection']}/>
          </div>
          <div className={'col-6'}>
            <Input label={'LABEL'} value={label} onChange={val => changeProp('label', val)} id={'label'} type={'text'}
                   placeholder={InferLabelFromName(name)}/>
          </div>
          <div className={'col-12'}>
            <Input label={'TEMPLATE TEXT'} onChange={val => changeProp('template', val)}
                   value={template ? template : ''}
                   id={'templateText'}
                   placeholder={''}
                   type={'text'}/>
          </div>
        </div>
      </div>)
    }
    default:
      return <h1>Error</h1>
  }
}

export default Property