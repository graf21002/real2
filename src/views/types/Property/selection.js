import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Input, Select, RadioInputs, OptionList } from '../../../components'
import { InferLabelFromName } from '../../../utils'

Selection.propTypes = {
  prop: PropTypes.object,
  changeProp: PropTypes.func
}
const SELECTION = 'selection'

function Selection ({ prop: { name, label, type, relevance, help, pattern, question, suggestions, allowFormatting, textMayContainLineBreaks, prefix }, changeProp }) {
  if (!suggestions) {
    suggestions = { name: 'anOptionListedBelow', list: [] }
  }
  if (!question) {
    question = 'dropdown'
  }
  let html = <h1>error</h1>
  if (question === 'dropdown') {
    html = <Select label={'Prefix'} id={'prefix'} value={prefix ? prefix : ''}
                   onChange={val => changeProp('prefix', val)}
                   values={suggestions.list}/>
  } else if (question === 'radio buttons') {
    html = <RadioInputs label={'Prefix'} name={'prefix'}
                        active={prefix ? prefix : ''}
                        values={suggestions.list}
                        handelChange={val => changeProp('prefix', val)}/>
  }
  return (
    <div className={'w-75'}>
      <div className={'row'}>
        <div className={'col-6'}>
          <Input label={'PROPERTY NAME'} value={name} onChange={val => changeProp('name', val)} id={'propertyName'}
                 type={'text'} spellcheck={false}/>
        </div>
        <div className={'col-6'}>
          <Select label={'DATA TYPE'} value={type} id={'dataType'} onChange={val => changeProp('type', val)}
                  values={['text', 'template', 'selection']}/>
        </div>
        <div className={'col-6'}>
          <Input label={'LABEL'} value={label} onChange={val => changeProp('label', val)} id={'label'} type={'text'}
                 placeholder={InferLabelFromName(name)}/>
        </div>
        <div className={'col-6'}>
          <Input label={'RELEVANCE (CONDITION)'} onChange={val => changeProp('relevance', val)}
                 value={relevance ? relevance : ''} id={'relevance'}
                 placeholder={'automatic'}
                 type={'text'}/>
        </div>
        <div className={'col-12'}>
          <Input label={'HELP TEXT'} onChange={val => changeProp('help', val)} value={help ? help : ''}
                 id={'helpText'}
                 placeholder={''}
                 type={'text'}/>
        </div>
        <div className={'col-12'}>
          <hr/>
          TEXT INTAKE
        </div>
        <div className={'col-6'}>
          <Input label={'PATTERN'} onChange={val => changeProp('pattern', val)}
                 value={pattern ? pattern : ''} id={'pattern'}
                 placeholder={''}
                 type={'text'}/>
        </div>
        <div className={'col-6'}>
          <div className={'form-group'}>
            <label>VALIDATION</label>
            <div className="form-check">
              <input className="form-check-input" type="checkbox" id="default"
                     onChange={() => changeProp('textMayContainLineBreaks', !textMayContainLineBreaks)}
                     checked={!!textMayContainLineBreaks}/>
              <label className="form-check-label" htmlFor="default">
                {InferLabelFromName('textMayContainLineBreaks')}
              </label>
            </div>
            <div className="form-check">
              <input className="form-check-input" type="checkbox" id="defaultCheck1"
                     onChange={() => changeProp('allowFormatting', !allowFormatting)}
                     checked={!!allowFormatting}/>
              <label className="form-check-label" htmlFor="defaultCheck1">
                {InferLabelFromName('allowFormatting (markdown)')}
              </label>
            </div>
          </div>
        </div>
        <div className={'col-4'}>
          <RadioInputs name={'suggestionsFrom'} label={'SUGGESTIONS FROM'}
                       active={suggestions.name}
                       values={['anOptionListedBelow']}
                       handelChange={val => changeProp('suggestions', { name: val, list: [] })}/>
        </div>
        <div className={'col-4'}>
          <Select label={'OPTION SOURCE'} value={type} id={'dataType'} onChange={val => changeProp('type', val)}
                  values={[]} disabled={true}/>
        </div>
        <div className={'col-4'}>
          <Select label={'QUESTION STYLE'} value={question} id={'dataType'}
                  onChange={val => changeProp('question', val)}
                  values={['dropdown', 'radio buttons']}/>
        </div>
        {suggestions.name === 'anOptionListedBelow' && <Fragment>
          <div className={'col-12'}>
            <OptionList lists={suggestions.list} handelChange={() => {
              let srt = prompt('Enter option?')
              if (srt) {
                changeProp('suggestions', { ...suggestions, list: [...suggestions.list, srt] })
              }
            }}/>
          </div>
          <div className={'col-12'}>
            PREVIEW
          </div>
          <div className={'col-3'}>
            {html}
          </div>
          {prefix && <div className={'col-3'}>
            RESULT
            <br/>
            {name}.name = "{prefix}"
          </div>}
        </Fragment>}

      </div>
    </div>
  )
}

export { SELECTION, Selection }