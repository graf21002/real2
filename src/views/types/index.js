import React, { useState } from 'react'
import { connect } from 'react-redux'
import Type from './type'
import { AddButton, Search, List } from '../../components'

import { changeTypeName, addPropOnType, changePropOnType } from '../../action'
import { active } from '../../action/modal'

function Types ({ types, change, addType, addProp, changeProp }) {
  let [search, setSearch] = useState('')
  let [active, setActive] = useState(0)
  let [activeProp, setActiveProp] = useState(0)
  return (
    <div className={'row'}>
      <div className={'col-2'}>
        TYPES
        <Search onSubmit={setSearch}/>
        <List
          items={search ? types.filter(type => type.name.toUpperCase().includes(search.toUpperCase())).map(type => type.name) : types.map(type => type.name)}
          onActive={act => {
            setActive(act)
            setActiveProp(0)
          }} active={active}/>
        <AddButton id={'addTypes'} label={'Add a new type'} onClick={addType}/>
      </div>
      {types[active] &&
      <Type type={types[active]} change={key => val => change(val, key, active)} addProp={() => addProp(active)}
            changeProp={(prop, key, val) => changeProp(active, prop, key, val)}
            setActiveProp={setActiveProp} active={activeProp}
      />}
    </div>)
}

const mapStateToProps = state => {
  let { types } = state
  return { types }
}

const mapDispatchToProps = dispatch => ({
  change: (val, key, index) => dispatch(changeTypeName(val, key, index)),
  addType: () => dispatch(active('newType')),
  addProp: index => dispatch(active('newProperty', index)),
  changeProp: (index, prop, key, val) => dispatch(changePropOnType(index, prop, key, val))
})

export default connect(mapStateToProps, mapDispatchToProps)(Types)