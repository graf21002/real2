import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'
import { AddButton, Input, Search } from '../../../components'

import Property from '../Property'

Type.propTypes = {
  changeName: PropTypes.func,
  addProp: PropTypes.func
}

function Type ({ type: { name, label, props }, change, addProp, active, changeProp, setActiveProp }) {
  let [search, setSearch] = useState('')
  return (
    <div className={'col-10'}>
      DETALIS
      <div className={'row bg-white  mt-2 mb-2'}>
        <div className={'col-12'}>
          <div className="row">
            <div className="col-3">
              <Input label={'OBJECT TYPE'} value={name} onChange={change('name')} type={'text'} id={'objType'}/>
            </div>
            <div className="col-3">
              <Input label={'LABEL (SINGULAR)'} value={label ? label : name} onChange={change('label')} type={'text'}
                     id={'labelType'}/>
            </div>
          </div>
        </div>
        <div className={'col-12'}>
          <ul className="nav nav-tabs text-uppercase" role="tablist">
            <li className="nav-item mr-2">
              <a className="nav-link active" role="tab" aria-selected="true">Properties</a>
            </li>
            <li className="nav-item  mr-2">
              <a className="nav-link" id="profile-tab" role="tab" aria-selected="false">Templates</a>
            </li>
            <li className="nav-item  mr-2">
              <a className="nav-link" id="contact-tab" role="tab" aria-selected="false">Lists</a>
            </li>
            <li className="nav-item  mr-2">
              <a className="nav-link" id="contact-tab" role="tab" aria-selected="false">Layouts</a>
            </li>
          </ul>
          <div className="tab-content" id="myTabContent">
            <div className="tab-pane fade show active p-2 bg-dart mb-2" role="tabpanel" aria-labelledby="home-tab">
              <div className={'row'}>
                <div className="col-3">
                  <Search onSubmit={setSearch}/>
                  <ul className="list-group w-100 rounded-0 mt-2 mb-2">
                    {search ? props.filter(prop => prop.name.toUpperCase().includes(search.toUpperCase())).map((prop, index) => (
                      <li
                        className={active === index ? 'list-group-item rounded pt-1 pb-1 pl-3 pr-3 mt-1 mb-1 bg-light' : 'list-group-item rounded pt-1 pb-1 pl-3 pr-3 mt-1 mb-1'}
                        key={prop.name + index}
                        onClick={() => setActiveProp(index)}>
                        <div className="d-flex justify-content-between">
                          <div><Icon type="font-colors"
                                     style={{ verticalAlign: 0 }}/> {prop.name}
                          </div>
                          <div>{prop.type}</div>
                        </div>
                      </li>)) : props.map((prop, index) => (
                      <li
                        className={active === index ? 'list-group-item rounded pt-1 pb-1 pl-3 pr-3 mt-1 mb-1 bg-light' : 'list-group-item rounded pt-1 pb-1 pl-3 pr-3 mt-1 mb-1'}
                        key={prop.name + index}
                        onClick={() => setActiveProp(index)}>
                        <div className="d-flex justify-content-between">
                          <div><Icon type="font-colors"
                                     style={{ verticalAlign: 0 }}/> {prop.name}
                          </div>
                          <div>{prop.type}</div>
                        </div>
                      </li>))}
                  </ul>
                  <AddButton id={'addProp'} onClick={addProp} label={'Add a new ...'}/>
                </div>
                <div className="col-9 p-2 bg-white">
                  {props[active] && <Property prop={props[active] ? props[active] : props[0]}
                                              changeProp={(key, val) => changeProp(active, key, val)}/>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Type