import React from 'react'
import PropTypes from 'prop-types'

Select.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func,
  values: PropTypes.array,
  disabled: PropTypes.bool
}
Select.defaultProps={
  disabled:false
}
function Select ({ id, label, value, onChange, values,disabled }) {
  return (
    <div className="form-group mt-2 mb-2">
      <label htmlFor={id}>{label}</label>
      <select className="form-control" id={id} value={value} onChange={e => onChange(e.target.value)}
              disabled={disabled}>
        {values.map(val => <option key={id + val}>{val}</option>)}
      </select>
    </div>
  )
}

export default Select