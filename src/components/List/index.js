import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string),
  onActive: PropTypes.func,
  active: PropTypes.number
}

function List ({ items, onActive, active }) {
  return (
    <ul className="list-group w-100 rounded-0 mt-2 mb-2">
      {items.map((item, index) => <li key={item + index} onClick={() => onActive(index)}
                                      className={active === index ? 'list-group-item rounded-0 bg-light' : 'list-group-item rounded-0'}>
        <Icon
          type="profile"
          className={'align-middle'}/> {item}
      </li>)}

    </ul>
  )
}

export default List