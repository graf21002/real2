import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'

AddButton.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  onClick: PropTypes.func,
}

function AddButton (props) {
  let { id, onClick, label } = props
  return (
    <div>
      <button id={id} onClick={onClick} type="button"
              className="btn btn-dart text-white rounded-circle btn-add mr-2 mb-2">
        <Icon style={{ marginTop: -4 }}
              type="plus" className={'align-middle'}/></button>
      <label htmlFor={id}>{label}</label>
    </div>
  )
}

export default AddButton