import React from 'react'
import { Icon } from 'antd'

function NavigationBar (props) {
  return (
    <div className={'container-fluid bg-white'}>
      <div className="d-flex justify-content-between align-items-center">
        <div className={'my-3'}>
          <div className={'d-inline mr-4'} style={{ fontWeight: 500 }}>Company names</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Estate plans</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Contractors</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Staff</div>
        </div>
        <div className={'my-3'}>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Collections</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase border-secondary border-bottom'}>Types</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Groups</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}>Users</div>
          <div className={'d-inline mr-4 font-weight-light text-uppercase'}><Icon style={{ verticalAlign: 0 }} type="user"/></div>
          <div className={'d-inline mr-4'} style={{ fontWeight: 500 }}>Andrew</div>
        </div>
      </div>
    </div>
  )
}

export default NavigationBar