import React from 'react'
import PropTypes from 'prop-types'

Input.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func,
  spellcheck: PropTypes.bool
}

function Input ({ type, value, id, onChange, label, placeholder, spellcheck }) {
  return (
    <div className="form-group mt-2 mb-2">
      <label htmlFor={id}>{label}</label>
      <input type={type} value={value} onChange={event => onChange(event.target.value)}
             className="form-control rounded-0" id={id} placeholder={placeholder}
             spellCheck={typeof spellcheck === "boolean" ? String(spellcheck) : null}/>
    </div>
  )
}

export default Input