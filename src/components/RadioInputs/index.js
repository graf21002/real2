import React from 'react'
import PropTypes from 'prop-types'

import { InferLabelFromName } from '../../utils'

RadioInputs.propTypes = {
  name: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.string),
  active: PropTypes.string,
  handelChange: PropTypes.func,
  label: PropTypes.string
}

function RadioInputs ({ label, name, values, active, handelChange }) {
  return (
    <div className={'form-group'}>
      <label>{label}</label>
      {values.map(val => (
        <div className="form-check" key={val}>
          <input className="form-check-input" type="radio" id={val}
                 value={val} name={name} checked={active === val} onChange={e => handelChange(e.target.value)}/>
          <label className="form-check-label" htmlFor={val}>
            {InferLabelFromName(val)}
          </label>
        </div>
      ))}
    </div>
  )
}

export default RadioInputs