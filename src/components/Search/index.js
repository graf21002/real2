import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'

Search.propTypes = {
  onSubmit: PropTypes.func
}

function Search (props) {
  let { onSubmit } = props
  const [value, setValue] = useState('')
  const submit = () => {
    if (value) {
      setValue('')
    }
  }
  const change = e => {
    setValue(e.target.value)
  }
  useEffect(() => {
    onSubmit(value)
  })
  return (
    <div className="input-group mt-2 mb-2">
      <input type="text" value={value} onChange={change} className="form-control rounded-0"
             placeholder="Search..."/>
      <div className="input-group-append">
        <button className="btn btn-outline-secondary btn-dark text-white rounded-0" onClick={submit} type="button">
          {value ? <Icon
            className={'align-middle'}
            type="close"/> : <Icon
            className={'align-middle'}
            type="search"/>}

        </button>
      </div>
    </div>
  )
}

export default Search